FROM pytorch/pytorch:1.10.0-cuda11.3-cudnn8-devel

RUN pip install jupyter scikit-learn scikit-image ninja matplotlib nibabel h5py scipy

RUN apt install nano -y
